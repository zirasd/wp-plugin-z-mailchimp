<?php
/*
Plugin Name: Z Mailchimp
Description: Shortcode and widget to create a mailchimp form.
Author: Thiago Locks
Author URI: http://github.com/thiagolcks
Version: 1.0
License: GPL2
Text Domain: z-mailchimp
*/

load_plugin_textdomain( 'z-mailchimp', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

wp_register_script( 'z-mailchimp', plugin_dir_url( __FILE__ ) . 'js/z-mailchimp.js', 'jquery', '1.0.0', true );
wp_localize_script(
	'z-mailchimp', 'ZMC', array(
	'ajaxurl' => admin_url( 'admin-ajax.php' ),
	'msg_invalid_email' => __( 'Your emails is invalid.', 'z-mailchimp' ),
	'msg_generic_error' => __( 'Sorry, but something was wrong.', 'z-mailchimp' ),
	'msg_success' => get_option( 'zmc_msg_success', __( 'Thanks, your email was subscribed.', 'z-mailchimp' ) ),
	)
);

require plugin_dir_path( __FILE__ ) . 'includes/class-z-mailchimp-shortcode.php';
require plugin_dir_path( __FILE__ ) . 'includes/class-z-mailchimp-widget.php';
if ( is_admin() ) {
	require plugin_dir_path( __FILE__ ) . 'includes/class-z-settings-fields.php';
	require plugin_dir_path( __FILE__ ) . 'includes/class-z-mailchimp-settings.php';
	$z_mailchimp_settings = new Z_Mailchimp_Settings( 'z-mailchimp' );
}