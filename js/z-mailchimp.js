(function(){
	var newsletter_form;

	newsletter_form = jQuery('.z-mc-form');

	if (newsletter_form.length > 0) {
	  newsletter_form.submit(function(e) {
	    var email, form, name, form_data;
	    e.preventDefault();
	    form = jQuery(this);
	    name = form.find('.z-mc-name');
	    email = form.find('.z-mc-email');
	    if (/^[a-zA-Z0-9][a-zA-Z0-9\._\-]+@([a-zA-Z0-9\._\-]+\.)[a-zA-Z-0-9]{2}/.exec(email.val())) {
	    	form_data = form.serialize() + '&action=z_mailchimp_subscribe';
	      jQuery.post(ZMC.ajaxurl, form_data, function(response) {
	        if (typeof response.data === 'string') {
	          alert(response.data);
	        } else if (response.success == true) {
	        	alert(ZMC.msg_success);
            email.val('');
            if ( name.length > 0 ) {
            	name.val('');
            }
          } else {
	          alert(ZMC.msg_generic_error);
	        }
	      });
	    } else {
	      alert(ZMC.msg_invalid_email);
	    }
	  });
	}
})();