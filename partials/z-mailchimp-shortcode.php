<form method="post" class="z-mc-form" >
	<?php wp_nonce_field( 'z_mailchimp', 'z_mailchimp_subscribe' ); ?>

	<?php if ( $atts['name'] !== 'false' ): ?>
		<input class="z-mc-name" type="text" name="name" placeholder="<?php echo esc_attr( $atts['name_placeholder'] ); ?>" />
	<?php endif; ?>

	<input class="z-mc-email" type="email" name="email" placeholder="<?php echo esc_attr( $atts['email_placeholder'] ); ?>" />
	<input class="z-mc-button" type="submit" name="submit" value="<?php echo esc_attr( $atts['button'] ) ?>" />
	
</form>