<div class="wrap">
	<h2><?php _e( 'Z Mailchimp - Configurations', $this->name ) ?></h2>
	<form action='options.php' method='post'>
				
		<?php
		settings_fields( 'zmc_settings_main' );
		do_settings_sections( 'zmc_settings_page' );
		submit_button();
		?>
		
	</form>
</div>