<?php

if ( ! class_exists( 'Z_Settings_Fields' ) ) {

	class Z_Settings_Fields {
		
		// Note the ID and the name attribute of the element should match that of the ID in the call to add_settings_field

		public function text( $args = array() ) {
			$value = esc_attr( get_option( $args['id'] ) );
			$html = '<input type="text" class="regular-text" id="' . $args['id'] . '" name="' . $args['id'] . '" value="' . $value . '" />';
			if ( $args['description'] ) $html .= '<p class="description"> '  . $args['description'] . '</p>';
			
			echo $html;
		}

		public function textarea( $args = array() ) {
			$value = esc_textarea( get_option( $args['id'] ) );
			$html = '<textarea class="large-text code" id="' . $args['id'] . '" name="' . $args['id'] . '" rows="10">' . $value . '</textarea>';
			if ( $args['description'] ) $html .= '<p class="description"> '  . $args['description'] . '</p>';
			
			echo $html;
		}

		public function checkbox( $args = array() ) {
			$value = get_option( $args['id'] );
			$html = '<input type="checkbox" class="regular-text" id="' . $args['id'] . '" name="' . $args['id'] . '" value="1" ' . checked( $value, '1' ) . ' />';
			if ( $args['description'] ) $html .= '<p class="description"> '  . $args['description'] . '</p>';
			
			echo $html;
		}

	}

}