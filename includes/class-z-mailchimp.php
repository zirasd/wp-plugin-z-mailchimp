<?php

if ( ! class_exists( 'Z_MailChimp' ) ) {

	/**
	 * Subscribe a user in a MailChimp List
	 */
	class Z_MailChimp {

		protected $_list_id = null;
		protected $_api = null;
		protected $_api_key = null;
		protected $_mc_args = null;

		public function __construct( $list_id, $api_key = null ) {
			$this->_list_id = $list_id;

			if ( $api_key ) {
				$this->set_api( $api_key );
			}
		}

		/**
		 * Get a API instance
		 * 
		 * @param string $api_key
		 */
		public function set_api( $api_key = null ) {
			if ( ! $this->_api ) {
				if ( $api_key ) {
					$this->_api_key = $api_key;
				}
				if ( ! $this->_api_key ) {
					throw new Exception( 'API key is not defined.', 1 );			
				}
				require_once plugin_dir_path( __FILE__ ) . 'vendor/MCAPI.class.php';
				$this->_api = new MCAPI( $api_key );
			}
		}

		/**
		 * Subscribe a user in a MailChimp preseted list
		 * 
		 * @param  string $email
		 * @param  array  $args
		 */
		public function subscribe( $email, $args = array() ) {
			$email = filter_var( $email, FILTER_SANITIZE_EMAIL );

			$args = array_merge(
				array(
					'double_optin'      => true,
					'format'            => 'html',
					'update_existing'   => true,
					'replace_interests' => false,
					'send_welcome'      => false,
				),
				$args
			);

			if ( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
				$this->set_api();
				$this->_set_name( $args );
				$result = $this->_api->listSubscribe( $this->_list_id, $email, $this->_mc_args, $args['format'], $args['double_optin'], $args['update_existing'], $args['replace_interests'], $args['send_welcome'] );
				if ( $this->_api->errorCode ){
					throw new Exception( $this->_api->errorMessage, $this->_api->errorCode );
				}
			} else {
				throw new Exception( 'Invalid E-mail', 502 );
			}
		}

		/**
		 * Unsubscribe a user in a MailChimp
		 * 
		 * @param  string $email
		 * @param  array  $args
		 */
		public function unsubscribe( $email, $args = array() ) {
			$email = filter_var( $email, FILTER_SANITIZE_EMAIL );

			$args = array_merge(
				array(
					'delete_member' => false,
					'send_goodbye'  => true,
					'send_notify'   => true,
				),
				$args
			);

			if ( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
				$this->set_api();
				$result = $this->_api->listUnsubscribe( $this->_list_id, $email, $args['delete_member'], $args['send_goodbye'], $args['send_notify'] );
				if ( $this->_api->errorCode ){
					throw new Exception( $this->_api->errorMessage, $this->_api->errorCode );
				}
			} else {
				throw new Exception( 'Invalid E-mail', 502 );
			}
		}

		/**
		 * Add some notes to user subscription
		 * 
		 * @param string $id     Some code identification for the note
		 * @param string $value  The note
		 */
		public function add_note( $id, $value ) {
			if ( ! emtpy( $id ) && ! empty( $value ) ) {
				$this->_mc_args['MC_NOTES'] = array( 'id' => $id, 'note' => $value );
			} else {
				throw new Exception( 'ID or note is empty.', 2 );
			}
		}

		/**
		 * Extract the user name from the args. It could come as fullname, or fname (first name) and lname (last name).
		 * 
		 * @param array $args  Array with the user name
		 */
		protected function _set_name( $args ) {
			$args = array_map( 'trim', $args );
			if ( isset( $args['fullname'] ) && ! empty( $args['fullname'] ) ) {
				$space = strpos( $args['fullname'], ' ' );
				if ( $space === FALSE ) {
					$this->_mc_args['FNAME'] = $args['fullname'];
				} else {
					$this->_mc_args['FNAME'] = trim( substr( $args['fullname'], 0, $space ) );
					$this->_mc_args['LNAME'] = trim( substr( $args['fullname'], $space ) );
				}
			} else {
				if ( isset( $args['fname'] ) && ! empty( $args['fname'] ) ) {
					$this->_mc_args['FNAME'] = $args['fname'];
				}
				if ( isset( $args['lname'] ) && ! empty( $args['lname'] ) ) {
					$this->_mc_args['LNAME'] = $args['lname'];
				}
			}
		}

		/**
		 * Add some interestests to a interest group
		 * 
		 * @param string $id
		 * @param array $new_groups 
		 */
		public function set_groups( $id, $new_groups ) {
			$group = $this->_get_group( $id );
			$this->_add_interest_groups( $group, $new_groups );
			$this->mc_args['GROUPINGS'] = array( array( 'id' => $group['id'], 'groups' => implode( ', ', $new_groups ) ) );
		}

		/**
		 * Register new interest groups to the current list
		 * 
		 * @param array $group
		 * @param array $new_groups
		 */
		protected function _add_interest_groups( $group, $new_groups ) {
			$possible_groups = array_map( function($i){ return $i['name']; }, $group['groups'] );
			$new_groups = array_diff( $new_groups, $possible_groups );
			foreach ( $new_groups as $group_name ) {
				$this->_api->listInterestGroupAdd( $this->_list_id, $group_name, $group['id'] );
			}
		}

		/**
		 * Get the group infos 
		 * 
		 * @param  string $id
		 * @return array()
		 */
		protected function _get_group( $id = null ) {
			$this->set_api();
			$all_groups = $this->_api->listInterestGroupings( $this->_list_id );
			if ( is_array( $all_groups ) ) {
				if ( $id ) {
					foreach ( $all_groups as $item ):
						if ( $item['id'] == $id ) return $item;
					endforeach;
				} else {
					return $all_groups[0];
				}
			} else {
				throw new Exception( 'There is no group registered.', 3 );
			}
		}

	}

}