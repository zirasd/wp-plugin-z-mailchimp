<?php

class Z_Mailchimp_Settings extends Z_Settings_Fields {

	private $name;

	public function __construct( $name ) {

		$this->name = $name;
		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );
		add_action( 'admin_init', array( $this, 'settings_init' ) );

	}

	/**
	 * Add the menu page
	 * 
	 * @since 1.0.0
	 */
	public function add_admin_menu() { 
		add_options_page( 'Z Mailchimp', 'Z Mailchimp', 'manage_options', 'zmc_settings_page', array( $this, 'options_page' ) );
	}

	/**
	 * Register the option fields
	 * 
	 * @since  1.0.0
	 */
	public function settings_init() { 

		add_settings_section(
			'zmc_settings_main_section', 
			'',
			null,
			'zmc_settings_page'
		);

		add_settings_field(
			'zmc_api_key', 
			__( 'API Key', $this->name ), 
			array( $this, 'text' ), 
			'zmc_settings_page', 
			'zmc_settings_main_section',
			array( 'id' => 'zmc_api_key' )
		);

		add_settings_field(
			'zmc_list_id', 
			__( 'List ID', $this->name ), 
			array( $this, 'text' ), 
			'zmc_settings_page', 
			'zmc_settings_main_section',
			array( 'id' => 'zmc_list_id' )
		);

		add_settings_field(
			'zmc_msg_success', 
			__( 'Success Message', $this->name ), 
			array( $this, 'text' ), 
			'zmc_settings_page', 
			'zmc_settings_main_section',
			array( 'id' => 'zmc_msg_success' )
		);

		register_setting( 'zmc_settings_main', 'zmc_api_key' );
		register_setting( 'zmc_settings_main', 'zmc_list_id' );
		register_setting( 'zmc_settings_main', 'zmc_msg_success' );
	}

	public function options_page() { 
		require plugin_dir_path( dirname( __FILE__ ) ) . 'partials/options-page.php';
	}

}