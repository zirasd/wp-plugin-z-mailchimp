<?php

class Z_Mailchimp_Shortcode {

	public function __construct() {
		add_shortcode( 'zmailchimp', array( $this, 'output' ) );
		add_action( 'wp_ajax_z_mailchimp_subscribe', array( $this, 'subscribe' ) );
		add_action( 'wp_ajax_nopriv_z_mailchimp_subscribe', array( $this, 'subscribe' ) );
	}

	public function subscribe() {
		if ( ! isset( $_POST['z_mailchimp_subscribe'] ) || ! wp_verify_nonce( $_POST['z_mailchimp_subscribe'], 'z_mailchimp' )  ) return;
		require plugin_dir_path( __FILE__ ) . 'class-z-mailchimp.php';
		$list = new Z_MailChimp( get_option( 'zmc_list_id' ), get_option( 'zmc_api_key' ) );
		$options = isset( $_POST['name'] ) ? array( 'fullname' => $_POST['name'] ) : array();
		try {
			$list->subscribe( $_POST['email'], $options );
			return wp_send_json_success();
		} catch( Exception $e ) {
			wp_send_json_error( $e->getMessage() );
		}
	}

	public function output( $atts ) {
		$atts = shortcode_atts(
			array(
				'name' => 'true',
				'name_placeholder' => __( 'Name', 'z-mailchimp' ),
				'email_placeholder' => __( 'E-mail', 'z-mailchimp' ),
				'button' => __( 'Subscribe', 'z-mailchimp' ),
			),
			$atts
		);
		if ( apply_filters( 'zmailchimp_load_script', true ) ) {
			wp_enqueue_script( 'z-mailchimp' );
		}
		$template = locate_template( 'z-mailchimp-shortcode.php' );
		ob_start();
		if ( $template ) {
			require $template;
		} else {
			require plugin_dir_path( dirname( __FILE__ ) ) . 'partials/z-mailchimp-shortcode.php';	
		}
		return ob_get_clean();
	}

}

$z_mailchimp_shortcode = new Z_Mailchimp_Shortcode();