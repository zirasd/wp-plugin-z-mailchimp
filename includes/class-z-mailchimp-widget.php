<?php

/**
 * new WordPress Widget format
 * Wordpress 2.8 and above
 * @see http://codex.wordpress.org/Widgets_API#Developing_Widgets
 */
class Z_Mailchimp_Widget extends WP_Widget {

	public function __construct() {
		$widget_ops = array( 'classname' => 'z-mailchimp', 'description' => 'Mailchimp form.' );
		parent::__construct( 'z-mailchimp', 'Z Mailchimp', $widget_ops );
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array  An array of standard parameters for widgets in this theme
	 * @param array  An array of settings for this widget instance
	 * @return void Echoes it's output
	 **/
	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		echo $before_widget;
		// echo $before_title;
		// echo 'Title'; // Can set this with a widget option, or omit altogether
		// echo $after_title;
		echo do_shortcode( '[zmailchimp]' );

		echo $after_widget;
	}


}

add_action( 'widgets_init', create_function( '', "register_widget( 'Z_Mailchimp_Widget' );" ) );